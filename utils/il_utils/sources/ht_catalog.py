#! /usr/bin/env python3
"""
Functions for the Hathi Trust catalog pages
"""

import argparse
import logging

import requests
import lxml.html

import re


class HathiCatalogEntry():

    def __init__(self, id, volume, title, source, fullview):
        self.id = id
        self.source = source
        self.volume = volume
        self.title = title
        self.fullview = fullview


def url_for_catalog_item(cid):
    return "https://catalog.hathitrust.org/Record/{}".format(cid)


class HathiCatalog():

    def __init__(self, id):

        self.id = id
        self.doc = None

    def _cache(self):

        url = url_for_catalog_item(self.id)
        req = requests.get(url)
        req.raise_for_status()

        doc = lxml.html.fromstring(req.content)

        self.doc = doc

    def get_volumes(self):

        self._cache()

        table = self.doc.cssselect('.viewability-table tbody')[0]

        vols = []

        for row in table.cssselect('tr'):

            link = row.cssselect('td:nth-child(1) > a')[0]

            classes = link.attrib['class'].split()
            fullview = "fulltext" in classes

            iid = link.attrib['href'].split("/")[-1]
            title = link.cssselect('.IndItem')[0].text.strip()
            source = row.cssselect('td:nth-child(2)')[0].text.strip()

            vol_num = None
            if title.startswith("v."):

                title = re.sub(r" no\.", ":no.", title)

                parts = title.split(maxsplit=1)

                vol_num = parts[0]
                title = parts[1] if len(parts) > 1 else ""

            vol = HathiCatalogEntry(
                iid, vol_num, title, source, fullview)

            vols.append(vol)

        return vols

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Get the volumes from a Hathi catalog')
    parser.add_argument('-i', '--id', required=True,
                        help='The catalog I, e.g. 000677224')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='show debugging information')
    parser.add_argument('-f', '--format',
                        default="{id}\t{vol}\t{title}\t{source}\t{fullview}",
                        help='Format string')

    args = parser.parse_args()

    log_level = logging.DEBUG if args.verbose else logging.INFO
    logging.basicConfig(level=log_level)


    cat = HathiCatalog(args.id)

    vols = cat.get_volumes()

    for v in vols:

        s = args.format.format(
            id = v.id,
            vol = v.volume if v.volume is not None else "",
            title = v.title,
            source = v.source,
            fullview = v.fullview
        )

        print(s)