from sys import path
import pywikibot
import logging

import argparse

from poke import index_poker

if __name__ == "__main__":

  parser = argparse.ArgumentParser(description='Poke pages in an index,')
  parser.add_argument('-i', '--index', type=str, nargs='+',
                      help='indexes to poke')
  parser.add_argument('-w', '--wiki', default='enwikisource',
                      help='the wikisource (needed for local files)')
  parser.add_argument('-v', '--verbose', action='count', default=0)

  args = parser.parse_args()

  pywikibot.output('Init PWB')
  logger = logging.getLogger('pywiki')
  logger.setLevel(logging.WARNING)

  if args.verbose > 1:
    logging.basicConfig(level=logging.DEBUG)
  elif args.verbose > 0:
    logging.basicConfig(level=logging.INFO)

  poker = index_poker.IndexPoker(wait=False)

  for index in args.index:
    poker.enqueue_index(args.wiki, index)

  poker.run()
  poker.stop(interrupt=False)