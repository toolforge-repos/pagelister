
INDEX_NS_MAP = {
  'sourceswiki': 106,
  'enwikisource': 106,
  'arwikisource': 106,
  'aswikisource': 106,
  'bewikisource': 106,
  'bnwikisource': 102,
  'brwikisource': 100,
  'cawikisource': 104,
  'cywikisource': 106,
  'dawikisource': 106,
  'dewikisource': 104,
  'elwikisource': 102,
  'eowikisource': 106,
  'eswikisource': 104,
  'etwikisource': 104,
  'fawikisource': 106,
  'frwikisource': 112,
  'guwikisource': 106,
  'hewikisource': 112,
  'hrwikisource': 104,
  'huwikisource': 106,
  'hywikisource': 106,
  'idwikisource': 102,
  'itwikisource': 110,
  'knwikisource': 106,
  'lawikisource': 106,
  'mlwikisource': 104,
  'mrwikisource': 106,
  'nlwikisource': 106,
  'nowikisource': 106,
  'plwikisource': 102,
  'pmswikisource': 104,
  'ptwikisource': 104,
  'rowikisource': 106,
  'ruwikisource': 106,
  'sawikisource': 106,
  'slwikisource': 104,
  'svwikisource': 108,
  'tewikisource': 106,
  'vecwikisource': 104,
  'viwikisource': 106,
  'zhwikisource': 106
}

def get_index_ns_for_wiki(wiki):

  try:
	  return INDEX_NS_MAP[wiki]
  except KeyError:
    pass

  return 252
