

import json
from sseclient import SSEClient as EventSource
import fnmatch
import logging
import os
import threading

from . import index_ns
from . import index_poker

class StreamWatcher():

  url = 'https://stream.wikimedia.org/v2/stream/recentchange'

  def __init__(self, handler, project_patterns, event_source) -> None:
      self.project_patterns = project_patterns
      self.handler = handler

      if event_source:
        self.event_source = event_source
      else:
        self.event_source = EventSource(self.url, last_id=None)

  def _match_project(self, wiki):
    if not self.project_patterns:
      return True

    for patt in self.project_patterns:
      if fnmatch.fnmatch( wiki, patt ):
        return True

    return False

  def _is_index_ns(self, change):

    ns = change['namespace']
    return ns == index_ns.get_index_ns_for_wiki(change['wiki'])

  def watch(self):
    logging.error("Start watch")
    for event in self.event_source:
      if event.event == 'message':
          try:
              change = json.loads(event.data)
          except ValueError:
              pass
          else:
            if (self._match_project( change['wiki']) and
              self._is_index_ns( change ) ):
              self.handler.handle_change(change)

    logging.error("End watch")


class IndexChangePokerHandler():
  """
  Handles changes on index pages by passing them to the background
  poke process if necessary

  This class handles the caching of indexes done before.
  """

  poked = {}

  def __init__(self) -> None:
    self.poker = index_poker.IndexPoker(wait=True)
    self.poker.run()

  def handle_change(self, change):
    index = change['title'].split(':', 1)[-1]

    # we've done this one
    if index in self.poked:
      return

    # dispatch to the index poker
    self.poker.enqueue_index(change['wiki'], index)

    # don't poke it again
    self.poked[index] = True

  def unpoke(self, index):
    """
    Forget that we poked this index
    """
    del self.poked[index]

class Poker():

  stream_watcher = None
  index_change_handler = IndexChangePokerHandler()

  def __init__(self) -> None:
      self.wikis = (os.getenv('THUMB_POKE_WIKIS') or 'enwikisource').split()
      print(os.getenv('THUMB_POKE_WIKIS'))

  def start_rc_watcher(self):

    self.stream_watcher = StreamWatcher(
      self.index_change_handler,
      self.wikis,
      event_source=None
    )

    thread = threading.Thread(target=self.stream_watcher.watch, daemon=True)
    thread.start()
    logging.error('end of start_rc_watcher')

  def unpoke(self, index):
    self.index_change_handler.unpoke(index)

