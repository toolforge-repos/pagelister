
# Local development

* Install Docker and docker-compose
* Create an `.env` file using `.env.example` as a guide
* `docker-compose up -[-d]`
* Should now be available at http://localhost:8080

# Configuration

Set through environment variables in `.env`

# Toolforge

## Enter the python k8s shell

* `webservice --backend=kubernetes python3.7 shell`

## Update the venv

* `python3 -m venv ~/www/python/venv`

## Clone the repo

* `cd www/python`
* `git clone http://phabricator.wikimedia.org/source/tool-pagelister.git iltools`

## Symlink the application to where it is required

* `ln -s iltools/tools/pagelister src`

## Create the .env file

* `cp .env.example .env`

And edit the key/secrets

## Install deps

* `pip install -r $HOME/www/python/src/requirements.txt`
* `pip install -e $HOME/www/python/iltools/utils`

# To update on Toolforge

* `cd www/python/iltools`
* `git pull`
* `webservice --backend=kubernetes python3.7 restart`

# Credits

* Favicon: Eva Kufulium (CC-BY-3.0)