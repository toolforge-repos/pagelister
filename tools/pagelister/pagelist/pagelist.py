from flask import Blueprint, current_app, request, jsonify
import flask

import traceback

import il_utils.sources.ia_source as IAS
import il_utils.sources.ht_source as HTS

pagelist = Blueprint('pagelist', __name__,
                     template_folder='templates',
                     url_prefix='/pagelist')


@pagelist.route('/')
def index():
    return flask.render_template('api_doc.html', title='Pagelister')


@pagelist.route('/v1/list', methods=['GET'])
def pagelist_handler():

    errors = []
    source = None

    if 'source' not in request.args:
        errors.append({'msg': 'No source field'})
    if 'id' not in request.args:
        errors.append({'msg': 'No id field'})

    if not errors:
        if request.args['source'] == 'ia':
            source = IAS.IaSource(request.args['id'])
        elif request.args['source'] == 'ht':
            ht_dapi = current_app.config['HT_DAPI']
            source = HTS.HathiSource(ht_dapi, request.args['id'])
        else:
            errors.append(
                {'msg': 'Unknown source: {}'.format(request.args['source'])})

    if source is not None:

        try:
            offset = request.args.get('offset', 0, int)
        except ValueError:
            offset = 0

        try:
            pl = source.get_pagelist()
            pl.clean_up()
        except Exception:
            current_app.logger.info('Failed to get pagelist from source:\n'
                                    + traceback.format_exc())
            errors.append({'msg': 'Failed to get pagelist from source',
                           "detail": traceback.format_exc()})

    if errors:
        response = jsonify({
            "errors": errors,
        })
        code = 500
    else:
        response = jsonify({
            'source': request.args['source'],
            'id': request.args['id'],
            'pagelist': pl.to_pagelist_tag(offset),
            'offset': offset
        })
        code = 200

    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('X-Robots-Tag', 'noindex')

    return response, code


@pagelist.route('/v1/images', methods=['GET'])
def images():
    """
    Get the list of images in a sequence.
    """

    errors = []
    source = None

    ht_dapi = current_app.config['HT_DAPI']

    if 'source' not in request.args:
        errors.append({'msg': 'No source field'})
    if 'id' not in request.args:
        errors.append({'msg': 'No id field'})

    if not errors:
        if request.args['source'] == 'ia':
            source = IAS.IaSource(request.args['id'])
        elif request.args['source'] == 'ht':
            ht_dapi = current_app.config['HT_DAPI']
            source = HTS.HathiSource(ht_dapi, request.args['id'])
        else:
            errors.append(
                {'msg': 'Unknown source: {}'.format(request.args['source'])})

    if source is not None:

        images = []

    if errors:
        response = jsonify({
            "errors": errors,
        })
        code = 500
    else:
        response = jsonify({
            'source': request.args['source'],
            'id': request.args['id'],
            'images': images,
        })
        code = 200

    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('X-Robots-Tag', 'noindex')

    return response, code
