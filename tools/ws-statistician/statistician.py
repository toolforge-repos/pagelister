
import luadata
import pywikibot

def get_monthname(lang, month):

    months = {
        'en': [
            "January", "February", "March", "April",
            "May", "June", "July", "August",
            "September", "October", "November", "December"
        ]
    }

    return months[lang][month - 1]

CAT_PATTS = {
    'en': 'Monthly Challenge ({monthname} {year})'
}

def get_category_title(lang, year, month):
    patt = CAT_PATTS[lang]

    monthname = get_monthname(lang, month)
    return patt.format(year=year, monthname=monthname)

class Statistician():

    def __init__(self, site) -> None:
        self.site = site

    def get_data_module(self, year, month):
        return pywikibot.Page(self.site,
            f'Module:Monthly Challenge/data/{year:d}-{month:02d}')

    def get_indexes(self, year, month):
        data_page = self.get_data_module(year, month)

        if not data_page.exists():
            raise ValueError(f'Data module does not exist for {year}-{month:02d}')

        d = luadata.unserialize(data_page.text)

        indexes = []
        for month in d['works']:
            for index in d['works'][month]:

                index = index.replace("\\'", "'")
                indexes.append(pywikibot.Page(self.site, 'Index:' + index))

        return indexes

    def get_category(self, year, month):
        return pywikibot.Category(
            self.site,
            get_category_title(self.site.lang, year, month)
        )