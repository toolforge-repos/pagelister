#! /usr/bin/env python3

import argparse

import os
import dotenv
import datetime

import mysql.connector
import pywikibot
import pywikibot.proofreadpage
import luadata
import threading
import concurrent.futures

import statistician

def sql_get_index_totals(conn, index_id):

    cursor = conn.cursor()

    sql = """
SELECT prh_status, COUNT(*) cnt
FROM pages
JOIN pr_history ON pages.page_id = pr_history.prh_page_id
JOIN ( SELECT prh_page_id, MAX(prh_rev_time) edit_time
       FROM pr_history
       GROUP BY prh_page_id ) last_time ON pr_history.prh_page_id = last_time.prh_page_id
                                        AND pr_history.prh_rev_time = last_time.edit_time
WHERE pages.page_index_id = %s
GROUP BY prh_status
;"""

    cursor.execute(sql, (index_id,))
    return cursor.fetchall()


def sql_get_index_totals_as_of(ih_conn, index_id, as_of):

    cursor = ih_conn.cursor()
    sql = """
SELECT prh_status, COUNT(*) cnt
FROM pages
JOIN pr_history ON pages.page_id = pr_history.prh_page_id
JOIN ( SELECT prh_page_id, MAX(prh_rev_time) edit_time
       FROM pr_history
       WHERE prh_rev_time <= %s
       GROUP BY prh_page_id ) last_time ON pr_history.prh_page_id = last_time.prh_page_id
                                        AND pr_history.prh_rev_time = last_time.edit_time
WHERE pages.page_index_id = %s
GROUP BY prh_status
;"""

    cursor.execute(sql, (as_of, index_id))
    return cursor.fetchall()


def sql_get_current_num_index_pages(mw_conn, index_id):

    cursor = mw_conn.cursor()

    sql = """
SELECT pr_count
FROM page
JOIN pr_index ON pr_page_id = page_id
WHERE page_id=%s
;"""

    cursor.execute(sql, (index_id,))
    return cursor.fetchone()[0]


def connect(db):

    user = os.getenv('MYSQL_USER')
    password = os.getenv('MYSQL_PASSWORD')
    db = user + db
    host = os.getenv('MYSQL_TOOLSDB_HOST')
    port = os.getenv('MYSQL_TOOLSDB_PORT')

    conn = mysql.connector.connect(
        user=user,
        host=host,
        password=password,
        port=port,
        database=db)

    return conn


def connect_mw(db):

    user = os.getenv('MYSQL_USER')
    password = os.getenv('MYSQL_PASSWORD')
    host = os.getenv('MYSQL_MW_HOST')
    port = os.getenv('MYSQL_MW_PORT')

    conn = mysql.connector.connect(
        user=user,
        host=host,
        password=password,
        port=port,
        database=db)

    return conn


def construct_lua_data_table(tdata, comment):

    return """--[=[
{}
]=]
return {}""".format(
        comment, tdata
    )


def check_accept(old, new, desc):

    if old == new:
        print("Text not changed: {}".format(desc))
        return False

    pywikibot.showDiff(old, new)

    choice = pywikibot.input_choice(
            'Do you want to accept these changes?',
            [('Yes', 'y'), ('No', 'n')],
            default='N')

    return choice == 'y'


def month_name(m):

    return ['January', 'February', 'March', 'April', 'May', 'June',
            'July', 'August', 'September', 'October', 'November',
            'December'][m - 1]


def zero_stats():
    return {
        'count': 0,
        'q0': 0,
        'q1': 0,
        'q2': 0,
        'q3': 0,
        'q4': 0,
    }


def update_page(site, page, data, summary, comment, dry_run, force, bot_flag):

    out_page = pywikibot.Page(site, page)

    t_lua = construct_lua_data_table(
        luadata.serialize(data, encoding="utf-8", indent="\t", indent_level=0),
        comment
    )

    if out_page.text == t_lua:
        print(page)
        print("No changes")
    else:
        if not dry_run:

            if force or check_accept(out_page.text, t_lua, "Category stats"):
                out_page.put(t_lua, summary=summary, botflag=bot_flag)
        else:
            print(page)
            print(t_lua)

class Updater():

    def __init__(self, db_name, mw_db_name) -> None:
        self.mw_db_name = mw_db_name
        self.db_name = db_name

        self.total_data = {
            'total': zero_stats(),
            'indexes': {}
        }

        self.daily_data = {
            'days': {}
        }

        self.datalock = threading.Lock()

    def process_index(self, index, timestamps):
        print(index)

        # new connection because this is threaded
        mw_conn = connect_mw(self.mw_db_name)
        conn = connect(self.db_name)

        num_pages = sql_get_current_num_index_pages(mw_conn, index.pageid)
        totals = sql_get_index_totals(conn, index.pageid)

        day_results = {}
        for t in timestamps:
            day_results[t] = sql_get_index_totals_as_of(conn, index.pageid,
                                                        t[1].totimestampformat())

        with self.datalock:
            idata = zero_stats()
            idata['count'] = num_pages

            print(f'Num pages {num_pages}')

            for r in totals:
                idata['q{}'.format(r[0])] = r[1]

            for k in ['q0', 'q1', 'q2', 'q3', 'q4', 'count']:
                self.total_data['total'][k] += idata[k]

            self.total_data['indexes'][index.title(with_ns=False)] = idata

            for t in timestamps:

                i = t[0]

                if i not in self.daily_data['days']:

                    self.daily_data['days'][i] = zero_stats()

                day_counts = day_results[t]

                self.daily_data['days'][i]['count'] += num_pages
                for r in day_counts:
                    self.daily_data['days'][i]['q{}'.format(r[0])] += r[1]

def main():

    parser = argparse.ArgumentParser(description='')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='show debugging information')
    parser.add_argument('-t', '--total-stats', action='store_true',
                        help='Update current index total stats')
    parser.add_argument('-d', '--daily-stats', action='store_true',
                        help='Update daily stats')
    parser.add_argument('-m', '--month', type=int,
                        help='The month (1-12')
    parser.add_argument('-y', '--year', type=int,
                        help='The year (YYYY)')
    parser.add_argument('-D', '--day', type=int,
                        help='The day (1-31)')
    parser.add_argument('-n', '--dry-run', action='store_true',
                        help='Dry run')
    parser.add_argument('-f', '--force', action='store_true',
                        help='Do not ask permission to write')
    parser.add_argument('-b', '--bot-flag', action='store_true',
                        help='Write pages with a bot flag')
    parser.add_argument('-T', '--threads', type=int, default=1,
                        help='number of threads to use for DB connections')
    args = parser.parse_args()

    # log_level = logging.DEBUG if args.verbose else logging.INFO
    # logging.basicConfig(level=log_level)

    if args.month and (args.month < 1 or args.month > 12):
        raise ValueError("Invalid month: {}".format(args.month))

    dotenv.load_dotenv()

    now = datetime.date.today()

    if args.month is None:
        args.month = now.month

    if args.year is None:
        args.year = now.year

    if args.day is None:
        args.day = now.day

    ym_str = '{}-{:02d}'.format(args.year, args.month)
    cat_stats_name = f'Module:Monthly Challenge category stats/data/{ym_str}'
    daily_stats_name = f'Module:Monthly Challenge daily stats/data/{ym_str}'
    mc_title = 'Wikisource:Community collaboration/Monthly Challenge/{m} {y}'.format(
        m=month_name(args.month),
        y=args.year
    )

    # DB names
    conn = '__idx_hist'
    mw_conn = 'enwikisource' + '_p'

    site = pywikibot.Site('en', 'wikisource')
    statn = statistician.Statistician(site)

    indexes = statn.get_indexes(args.year, args.month)

    timestamps = []
    day = datetime.timedelta(days=1)
    start = pywikibot.Timestamp(args.year, args.month, 1)
    for i in range(args.day + 1):

        day = datetime.timedelta(days=1)
        timestamps.append((i, start))
        start += day

    updater = Updater(conn, mw_conn)

    with concurrent.futures.ThreadPoolExecutor(args.threads) as executor:
        futures = []
        for index in indexes:
            futures.append(executor.submit(
                updater.process_index,
                index, timestamps))

        concurrent.futures.wait(futures)
        for future in futures:
            future.result()

    if args.total_stats:
        update_page(
            site, cat_stats_name, updater.total_data,
            comment=f'Automatically generated data for indexes in [[{mc_title}]]',
            summary=f'Updating current statistics for indexes in [[{mc_title}]]',
            dry_run=args.dry_run,
            force=args.force,
            bot_flag=args.bot_flag)

    if args.daily_stats:
        update_page(
            site, daily_stats_name, updater.daily_data,
            comment=f'Automatically generated daily data for indexes in [[{mc_title}]]',
            summary=f'Updating current daily statistics for indexes in [[{mc_title}]]',
            dry_run=args.dry_run,
            force=args.force,
            bot_flag=args.bot_flag)


if __name__ == "__main__":
    main()

