#! /usr/bin/env bash
#
## Update the Monthly Challenge statistics
#
# update-db: update the tables for the current month's category
# write-stats: generate and write statistics to the wiki

set -ex

m=$(LANG=en_us_88591; date "+%B")
y=$(date "+%Y")
mnum=$(date "+%m")

tmrD=$(date --date=tomorrow "+%d")
tmrM=$(date --date=tomorrow "+%B")
tmrMNum=$(date --date=tomorrow "+%m")
tmrYear=$(date --date=tomorrow "+%Y")

WS_STAT_TOOL_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

# source the virtualenv
if [[ $WS_STAT_VENV ]]
then
    source "${WS_STAT_VENV}/bin/activate"
fi

python --version

function update_month_db {
    echo "Running database update script for: $1 $2"

    python $WS_STAT_TOOL_DIR/update_db.py \
        --year "$1" --month "$2"
}

cmd=$1

case $cmd in
"update-db")
    update_month_db $y $mnum

    # run updates for next months works
    if [[ "$tmrD" == "01" ]]
    then
        update_month_db $tmrYear $tmrMNum
    fi
    ;;
"write-stats")
    echo "Running wiki stats update for ${mnum}-${y}"

    python $WS_STAT_TOOL_DIR/gen_stats.py \
        --month "$mnum" --year "$y" \
        --force --bot-flag --total-stats --daily-stats

    # on the last day of the month
    if [[ "$tmrD" == "01" ]]
    then
        echo "Running month rollover for ${tmrYear}-${tmrMNum}"

        # generate the NEXT months daily stats (will be empty daily tallies
        # and will also update the totals)
        python $WS_STAT_TOOL_DIR/gen_stats.py \
            --month "$tmrMNum" --year "$tmrYear" \
            --day 0 \
            --force --bot-flag --total-stats --daily-stats
    fi
    ;;
esac
