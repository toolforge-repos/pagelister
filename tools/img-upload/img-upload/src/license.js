

function validateLicense( s ) {

	if ( s.length < 6 ) {
		return false;
	}

	if ( /deathyear\s*=\s*YYYY/.test( s ) ) {
		return false;
	}

	// doesn't look like any template(?)
	if ( !/\{\{.*\}\}/.test( s ) ) {
		return false;
	}

	// there's nothing obviously WRONG with it
	return true;
}

module.exports = {
	validateLicense,
};
