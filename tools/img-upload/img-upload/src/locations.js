const cities = [
  'London', 'Cambridge', 'Oxford', 'Edinburgh', 'Glasgow', 'Manchester', 'Liverpool',
  'New York', 'Philadelphia', 'Boston', 'Ann Arbor',
  'Paris',
  'Berlin', 'Frankfurt', 'Stuttgart', 'Jena',
  'Hong Kong', 'Shanghai',
  'Calcutta', 'Bombay', 'Delhi'
];

module.exports = [
  cities
];