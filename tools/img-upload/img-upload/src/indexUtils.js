

function stripWikicode( t ) {
	return t
		.replace( /\[\[.*?\|(.*)\]\]/g, '$1' )
		.replace( /\[\[(?:\w+:)?(.*?)\]\]/g, '$1' )
		.replace( /'''?/g, '' );
}


/*
 * Return a promise
 */
function getIndexFields( wiki, index ) {
	// strip NS if any
	index = index.replace(/^.*?:/, '');

	const params = {
		format: 'json',
		formatversion: 2,
		origin: '*',
		action: 'parse',
		prop: 'wikitext',
		contentformat: 'application/json',
		page: 'Index:' + index
	};

	return fetch(wiki + '/w/api.php?' + new URLSearchParams(params))
		.then( res => res.json() )
		.then( data => {

			if ( !data.parse ) {
				return null;
			}

			let indexData;
			try {
				indexData = JSON.parse( data.parse.wikitext );
			} catch {
				return null;
			}

			const fields = indexData.fields || {};

			// TODO: this mapping will not work for non enWS
			// probably the API should be returning a mapping somewhere
			//
			// But the real solution is to use Wikidata
			let ret = {
				title: fields.Title,
				author: fields.Author,
				year: fields.Year,
				illustrator: fields.Illustrator,
			};

			// the JSON contains raw Wiki code
			// so attempt to strip it.
			Object.keys( ret ).map( function( key ) {
				ret[ key ] = stripWikicode( ret[ key ] );
			});

			return ret;
		} );
}


module.exports = {
	getIndexFields,
};
