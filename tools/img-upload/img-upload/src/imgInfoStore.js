
function getKey(form) {
	const key = 'details' + "_" + form.imgType + "_" + form.index;
	return key;
}

function storeUploadDetails(form) {

	let toStore = {
		imgType: form.imgType,
		index: form.index,
		title: form.title,
		source: form.source,
		date: form.date,
		dateInTitle: form.dataInTitle,
		author: form.author,
		location: form.location,
		license: form.license,
		categories: form.categories,
	}

	const key = getKey(form);

	let data= getAllData();
	data[key] = toStore;

	localStorage.setItem('upload_details', JSON.stringify(data) );
}

function getAllData() {
	let data;
	try {
		data = JSON.parse(localStorage.getItem('upload_details'));
	} catch (e) {
		data = {};
	}

	if ( !data ) {
		data = {};
	}
	return data;
}

function getStoredDetails(form) {

	const key = getKey(form);
	const data = getAllData();

	const stored = data[key];

	if ( stored ) {
		form.imgType = stored.imgType || 'illustration';
		form.title = stored.title || '';
		form.source = stored.source || '';
		form.date  = stored.date || '';
		form.dataInTitle  = !!stored.dataInTitle;
		form.author  = stored.author || '';
		form.license  = stored.license || '';

		if ( stored.categories && stored.categories.length > 0 ) {
			form.categories  = stored.categories;
		}
		form.location  = stored.location || '';
	}
}

module.exports = {
	storeUploadDetails,
	getStoredDetails,
};
