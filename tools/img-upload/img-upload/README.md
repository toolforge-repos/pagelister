# img-upload

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## On toolforge ##


### First run ###

```
webservice --backend=kubernetes node10 shell
cd ~/iltools/tools/img-upload/img-upload/
npm install
npm run build
ln -s ~/www/static ~/iltools/tools/img-upload/img-upload/dist
```

App now available at https://ws-image-uploader.toolforge.org/

### Update ###

```
webservice --backend=kubernetes node10 shell
cd ~/iltools/tools/img-upload/img-upload/
npm run build
```