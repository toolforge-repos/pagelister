from flask import Flask, jsonify
import mwoauth
import mwoauth.flask
import flask
import os
from flask_cors import CORS, cross_origin

import client.client

from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())

# configuration
DEBUG = True

# instantiate the app
app = Flask(__name__)
app.config.from_object(__name__)

app.secret_key = os.environ.get("APP_SECRET_KEY")

cors = CORS(app, resources={r"/foo": {"origins": "*"}})
app.config['CORS_HEADERS'] = 'Content-Type'


# Construct a "consumer" from the key/secret provided by MediaWiki

# beta commons
consumer_token = mwoauth.ConsumerToken(
    os.environ.get('OAUTH_CONSUMER_KEY'),
    os.environ.get('OAUTH_CONSUMER_SECRET')
)

flask_mwoauth = mwoauth.flask.MWOAuth(
    os.environ.get('WIKI_URL'),
    consumer_token,
    default_next="client_bp.index",
    user_agent="WS Image Upload server")
app.register_blueprint(flask_mwoauth.bp)


@app.route("/")
def index():
    return client.client.index()


def csrf_token(session):
    if not hasattr(session, 'csrf_token'):
        response = session.get(action='query',
                               meta='tokens')
        session.csrf_token = response['query']['tokens']['csrftoken']
    return session.csrf_token


def upload(session, file_object, file_name, wikitext, comment):
    token = csrf_token(session)

    response = session.post(action='upload',
                            filename=file_name,
                            upload_file=file_object,
                            token=token,
                            comment=comment,
                            text=wikitext)

    return response['upload']['result'] == 'Success'


@app.route('/identify', methods=['GET'])
@cross_origin()
def identify():
    """
    Provide information about the currently logged-in user.
    This doesn't trigger an OAuth process, it's mostly just to figure out
    if the user is logged in or not
    """

    ident = flask_mwoauth.identify()

    data = {
        'username': ident['username'] if ident else None,
        'beta': os.environ.get('USE_BETA_CLUSTER') == 'true'
    }

    return jsonify(data)


@app.route('/upload', methods=['POST'])
@mwoauth.flask.authorized
@cross_origin()
def upload_file():
    # check if the post request has the file part
    if 'file' not in flask.request.files:
        print('No file part')
        return jsonify({
            'success': False,
            'code': 'no-file-part',
            'reason': 'No file in form data'
        }), 400, {'ContentType': 'application/json'}
    file = flask.request.files['file']

    # If the user does not select a file, the browser submits an
    # empty file without a filename.
    if file.filename == '':
        return jsonify({
            'success': False,
            'code': 'no-file',
            'reason': 'No selected file'
        }), 400, {'ContentType': 'application/json'}

    useBeta = os.environ.get('USE_BETA_CLUSTER') == 'true'

    url = "https://"

    if flask.request.form['wiki'] == 'commons.wikimedia':
        url += "commons.wikimedia"
    elif flask.request.form['wiki'].endswith('wikisource'):
        url += flask.request.form['wiki']
    else:
        raise ValueError(f"Unknown wiki {flask.request.form['wiki']}")

    if useBeta:
        url += ".beta.wmflabs"

    url += ".org"

    filename = flask.request.form['file_name']
    wikitext = flask.request.form['description']

    wikitext = wikitext.strip() + "\n[[Category:Uploaded with Wikisource Image Uploader]]"

    wiki_session = flask_mwoauth.mwapi_session(url)

    summary = "Upload file with Wikisource File Uploader"

    success = upload(wiki_session, file, filename, wikitext, summary)

    if not success:
        return jsonify({'success': False}), 500, {'ContentType': 'application/json'}

    return jsonify({'success': True}), 200, {'ContentType': 'application/json'}


app.register_blueprint(client.client.client_bp)


if __name__ == '__main__':
    app.run(debug=True)
